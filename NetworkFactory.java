package com.way.app.network.factory;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.way.app.application.WayApplication;
import com.way.app.multi_language.MultiLanguageSupport;
import com.way.app.network.services.NetworkManagerService;
import com.way.app.usercards.model.CommonResponse;
import com.way.app.util.Log;
import com.way.app.util.TLSSocketFactory;
import com.way.app.util.Utils;

import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.ConnectionSpec;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import okio.BufferedSource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import way.com.BuildConfig;

/**
 * NetworkFactory
 */

public class NetworkFactory {
    public static final int TYPE_WIFI = 1;
    public static final int TYPE_MOBILE = 2;
    public static final int TYPE_NOT_CONNECTED = 0;
    private static Retrofit retrofit;
    private static NetworkFactory mNetworkFactory;
    private static OkHttpClient mOkHttpClient;
    private final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    public MultiLanguageSupport multiLanguageSupport;
    private Context mContext;

    private NetworkFactory(Context context) {

        mContext = context;
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

//            OkHttpClient okHttpClient1 = new OkHttpClient();
//            okHttpClient1.interceptors().add(httpLoggingInterceptor);


        Interceptor refreshTokenInterceptor = RefreshTokenInterceptor.getRefreshTokenInterceptorInstance(mContext);

//        OkHttpClient okHttpClient;
        if (BuildConfig.LOGGING) {

                mOkHttpClient = enableTls12OnPreLollipop(new OkHttpClient.Builder()
                        .readTimeout(180, TimeUnit.SECONDS)
                        .addInterceptor(refreshTokenInterceptor)
                        .addInterceptor(httpLoggingInterceptor)
                        .connectTimeout(60, TimeUnit.SECONDS)).build();




        } else {
                mOkHttpClient = enableTls12OnPreLollipop(new OkHttpClient.Builder()
                        .readTimeout(180, TimeUnit.SECONDS)
                        .addInterceptor(refreshTokenInterceptor)
                        .connectTimeout(60, TimeUnit.SECONDS))
                        .build();

        }


        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(mOkHttpClient)
                .baseUrl(BuildConfig.API_URL)
                .build();

    }

    private static OkHttpClient.Builder enableTls12OnPreLollipop(OkHttpClient.Builder client) {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 22) {
            try {
                SSLContext sc = SSLContext.getInstance("TLSv1.2");
                sc.init(null, null, null);

                TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(
                        TrustManagerFactory.getDefaultAlgorithm());
                trustManagerFactory.init((KeyStore) null);
                TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
                if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
                    throw new IllegalStateException("Unexpected default trust managers:"
                            + Arrays.toString(trustManagers));
                }
                X509TrustManager trustManager = (X509TrustManager) trustManagers[0];

                client.sslSocketFactory(new TLSSocketFactory(sc.getSocketFactory()), trustManager);

                ConnectionSpec cs = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                        .tlsVersions(TlsVersion.TLS_1_2)
                        .build();

                List<ConnectionSpec> specs = new ArrayList<>();
                specs.add(cs);
                specs.add(ConnectionSpec.COMPATIBLE_TLS);
                specs.add(ConnectionSpec.CLEARTEXT);

                client.connectionSpecs(specs);
            } catch (Exception exc) {
                Log.e("OkHttpTLSCompat", "Error while setting TLS 1.2", exc);
            }
        }

        return client;
    }

    public static void cancelRunningRequestes() {
        mOkHttpClient.dispatcher().cancelAll();
    }

    public static NetworkFactory getNetworkFactoryInstance(Context context) {

        if (mNetworkFactory == null) {
            mNetworkFactory = new NetworkFactory(context.getApplicationContext());
        }
        return mNetworkFactory;
    }

    public static boolean isNetWorkAvailable(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            if (connMgr == null) {
                return false;
            }
            if (connMgr.getActiveNetworkInfo() != null
                    && connMgr.getActiveNetworkInfo().isAvailable()
                    && connMgr.getActiveNetworkInfo().isConnected()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static String getNetworkClass(Context context) {
        if (context == null) {
            return "";
        }
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = null;
        if (cm != null) {
            info = cm.getActiveNetworkInfo();
        }
        if(info==null || !info.isConnected())
            return ""; //not connected
        if(info.getType() == ConnectivityManager.TYPE_WIFI)
            return "WIFI";
        if(info.getType() == ConnectivityManager.TYPE_MOBILE){
            int networkType = info.getSubtype();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN: //api<8 : replace by 11
                    return "2G";
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B: //api<9 : replace by 14
                case TelephonyManager.NETWORK_TYPE_EHRPD:  //api<11 : replace by 12
                case TelephonyManager.NETWORK_TYPE_HSPAP:  //api<13 : replace by 15
                case TelephonyManager.NETWORK_TYPE_TD_SCDMA:  //api<25 : replace by 17
                    return "3G";
                case TelephonyManager.NETWORK_TYPE_LTE:    //api<11 : replace by 13
                case TelephonyManager.NETWORK_TYPE_IWLAN:  //api<25 : replace by 18
                case 19:  //LTE_CA
                    return "LTE";
                default:
                    return "";
            }
        }
        return "";
    }

    public NetworkManagerService getNetworkManagerService() {
        return retrofit.create(NetworkManagerService.class);
    }

    public int getConnectivityStatus(Context context) {
        if (context != null) {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (null != activeNetwork) {
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                    return TYPE_WIFI;
                }

                if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                    return TYPE_MOBILE;
                }
            }
            return TYPE_NOT_CONNECTED;
        } else {
            return TYPE_NOT_CONNECTED;
        }
    }

    public RequestBody createRequestBody(String jsonString) {
        return RequestBody.create(JSON, jsonString);
    }

    public void makeGetRequest(String url
            , Callback<ResponseBody> mResponseBodyCallback
            , Map<String, String> headerMap) {

//        Log.e("GetRequest", "makeGetRequest: "+ url);
        Call<ResponseBody> responseCallback = null;
        if (headerMap != null) {
            responseCallback = getNetworkManagerService().
                    makeGetRequest(url, headerMap);
        } else {
            responseCallback = getNetworkManagerService().makeGetRequest(url);
        }
        responseCallback.enqueue(mResponseBodyCallback);

    }

    public void makeGetRequest(String url
            , Callback<ResponseBody> mResponseBodyCallback) {

        Call<ResponseBody> responseCallback;
        responseCallback = getNetworkManagerService().makeGetRequest(url);
        responseCallback.enqueue(mResponseBodyCallback);
    }

//    public void makeDeleteRequest(String url
//            , String body
//            , Callback<ResponseBody> mResponseBodyCallback
//            , Map<String, String> headerMap) {
//
//        Call<ResponseBody> responseCallback = getNetworkManagerService().
//                makeDeleteRequest(url, createRequestBody(body), headerMap);
//        responseCallback.enqueue(mResponseBodyCallback);
//
//    }

    public void makePostRequest(String url
            , String body
            , Callback<ResponseBody> mResponseBodyCallback
            , Map<String, String> headerMap) {

        Call<ResponseBody> responseCallback = getNetworkManagerService().
                makePostRequest(url, createRequestBody(body), headerMap);
        responseCallback.enqueue(mResponseBodyCallback);

    }

    public Call<ResponseBody> makePostRequestSynchronous(String url
            , String body
            , Callback<ResponseBody> mResponseBodyCallback
            , Map<String, String> headerMap) {

        return getNetworkManagerService().
                makePostRequest(url, createRequestBody(body), headerMap);

    }

    public void makePutRequest(String url
            , String body
            , Callback<ResponseBody> mResponseBodyCallback
            , Map<String, String> headerMap) {

        Call<ResponseBody> responseCallback = getNetworkManagerService().
                makePutRequest(url, createRequestBody(body), headerMap);
        responseCallback.enqueue(mResponseBodyCallback);

    }

    public void makeDELETERequest(String path, String body
            , Callback<ResponseBody> mResponseBodyCallback
            , Map<String, String> headerMap) {

        Call<ResponseBody> responseCallback = getNetworkManagerService().
                makeDeleteRequest(path, createRequestBody(body), headerMap);
        responseCallback.enqueue(mResponseBodyCallback);

    }

    /**
     * upload Profile picture
     *
     * @param fileToUpload get fileto Uload
     * @param userID       get UserID
     */
    public void uploadPhoto(String userID, MultipartBody.Part fileToUpload, Callback<ResponseBody> mResponseBodyCallback
            , Map<String, String> headerMap) {

        Call<ResponseBody> imageUploadCallback = getNetworkManagerService().uploadImage(
                userID, fileToUpload, headerMap);
        imageUploadCallback.enqueue(mResponseBodyCallback);
    }

    public Map<String, String> createHeaderMap(String authorizationToken
            , String contentType) {
        String version = null;
        int android_os ;

       try {
            version = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionName;
       } catch (PackageManager.NameNotFoundException e) {
           e.printStackTrace();
       }
       android_os =  android.os.Build.VERSION.SDK_INT ;
        String deviceName = android.os.Build.MODEL;
        String deviceManf = android.os.Build.MANUFACTURER;

        Map<String, String> headerMap = new HashMap<>();
        if (!contentType.equals(Utils.NO_CONTENTTYPE)) {
            headerMap.put("Content-Type", contentType);
        }
        headerMap.put("Authorization", authorizationToken);


        String deviceUuid = "";
        try {
            deviceUuid = ((WayApplication) mContext.getApplicationContext()).getDeviceUuid();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!TextUtils.isEmpty(deviceUuid) && !deviceUuid.equals("null")) {
            headerMap.put("user-agent-uuid",deviceUuid);
        }

       // headerMap.put("User-agent","Android App/"  + android_os + "/" + version ); //+ version + "/" + android_os
        headerMap.put("User-Agent-App","Android App");
        // for user agent as per the cients req
       headerMap.put("User-Agent-App-Version","" + version);
       headerMap.put("User-Agent-OS-Version",""  + android_os);
       headerMap.put("User-Agent-Hardware",""+ deviceManf + "/" + deviceName );
        Log.e("Authorization: ", ""+ deviceName);
        Log.e("Authorization: ", ""+ deviceManf);
        return headerMap;

    }

    public static String getErrorMessage(Response<ResponseBody> response) {
        String errorMessage = null;
        try {

            if (response.errorBody() != null) {

                BufferedSource source = response.errorBody().source();
                source.request(Long.MAX_VALUE); // Buffer the entire body.
                Buffer buffer = source.buffer();


                String errorBody = buffer.clone().readUtf8();

                if (errorBody != null) {

                    CommonResponse errorResponse = new Gson().fromJson(errorBody,
                            CommonResponse.class);

                    if (errorResponse != null &&
                            !TextUtils.isEmpty(errorResponse.getMessage())) {
                        errorMessage =  errorResponse.getMessage();

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return errorMessage;
    }
    public static boolean getErrorMessageNoInventory(Response<ResponseBody> response) {
        boolean noInventory = false;
        try {

            if (response.errorBody() != null) {

                String errorBody = response.errorBody().string();

                if (errorBody != null) {

                    CommonResponse errorResponse = new Gson().fromJson(errorBody,
                            CommonResponse.class);

                    if (errorResponse != null ) {
                        noInventory = errorResponse.isNoInventory();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return noInventory;
    }
    public static String getErrorMessageNoInventoryMassage(Response<ResponseBody> response) {
        String errorMessage = null;
        try {

            if (response.errorBody() != null) {

                String errorBody = response.errorBody().string();

                if (errorBody != null) {

                    CommonResponse errorResponse = new Gson().fromJson(errorBody,
                            CommonResponse.class);

                    if (errorResponse != null &&
                            !TextUtils.isEmpty(errorResponse.getMessage())) {
                        errorMessage =  errorResponse.getMessage();

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return errorMessage;
    }
}
