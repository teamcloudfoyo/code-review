package com.way.app.reviews.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.way.app.dashboard.view.activity.DashboardActivity;
import com.way.app.reviews.adapter.ReviewListingAdapter;
import com.way.app.reviews.model.ReviewResponse;
import com.way.app.reviews.service.ReviewListRepositoryImpl;
import com.way.app.reviews.service.repository.ReviewListRepository;
import com.way.app.reviews.view.viewmodel.ReviewListViewModel;
import com.way.app.widget.CustomErrorDialog;

import way.com.R;
import way.com.databinding.FragmentReviewListBinding;

import static android.view.WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING;

public class ReviewListFragment extends Fragment {

    private ReviewListViewModel reviewListViewModel;
    private ReviewListingAdapter reviewListingAdapter;
    private FragmentReviewListBinding fragmentReviewListBinding;
    private Observer<ReviewResponse> reviewResponseObserver;
    private Observer<String> reviewErrorObserver;

    public static ReviewListFragment newInstance() {
        return new ReviewListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set this flag to avoid showing bottom bar above the keyboard
        if (getActivity()!=null) {
            getActivity().getWindow().setSoftInputMode(SOFT_INPUT_ADJUST_NOTHING);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        fragmentReviewListBinding = FragmentReviewListBinding.inflate(inflater, container, false);
        return fragmentReviewListBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        reviewListViewModel = ViewModelProviders.of(this).get(ReviewListViewModel.class);
        ReviewListRepository reviewListRepository = new ReviewListRepositoryImpl(this.getContext());
        reviewListViewModel.setReviewListRepository(reviewListRepository);
        fragmentReviewListBinding.setReviewListViewModel(reviewListViewModel);
        fragmentReviewListBinding.setLifecycleOwner(this);

        initToolbar();
        initRecyclerView();
        reviewResponseObserver = reviewResponse -> {
            fragmentReviewListBinding.shimmerViewContainer.setVisibility(View.GONE);

            if (reviewResponse != null) {

                if (reviewResponse.getReviews() != null && reviewResponse.getReviews().size() > 0) {
                    reviewListingAdapter.addData(reviewResponse.getReviews());
                } else {
                    if (reviewListingAdapter.getItemCount() == 0) {
                        fragmentReviewListBinding.rvReviewList.setVisibility(View.GONE);
                    }
                }
            }
        };
        reviewErrorObserver = errorMessage -> {
            fragmentReviewListBinding.shimmerViewContainer.setVisibility(View.GONE);
            new CustomErrorDialog().showAlert(getFragmentManager(), errorMessage);
        };

        reviewListViewModel.getReviewLiveData().observe(this, reviewResponseObserver);

        getReviewHistory();
    }

    private void initToolbar() {
        fragmentReviewListBinding.toolbar.imageIcon.setImageResource(R.mipmap.back_otp);
        fragmentReviewListBinding.toolbar.imageIcon.setOnClickListener(v -> {
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
        });
        if (getActivity()!=null) {
            fragmentReviewListBinding.tvHeader.setText(getActivity().getResources().getString(R.string.hear_what_people_say_about_way));
        }
    }

    private void initRecyclerView() {
        reviewListingAdapter = new ReviewListingAdapter((DashboardActivity) getActivity());
        fragmentReviewListBinding.rvReviewList.setAdapter(reviewListingAdapter);
    }

    /**
     * Call API to load results of review list
     */
    private void getReviewHistory() {
        reviewListViewModel.getErrorMessage().observe(this, reviewErrorObserver);
    }

}
