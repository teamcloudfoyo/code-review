package com.way.app.reviews.service.repository;

import androidx.lifecycle.LiveData;

import com.way.app.reviews.model.ReviewResponse;

public interface ReviewListRepository {

    LiveData<String> getErrorMessage();

    LiveData<ReviewResponse> getReviewList();

}
