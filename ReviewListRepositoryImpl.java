package com.way.app.reviews.service;

import android.content.Context;
import android.text.TextUtils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.way.app.multi_language.MultiLanguageSupport;
import com.way.app.network.factory.NetworkFactory;
import com.way.app.network.listener.OnNetworkNotConnectedListener;
import com.way.app.reviews.model.ReviewResponse;
import com.way.app.reviews.service.repository.ReviewListRepository;
import com.way.app.util.UILabelsKeys;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewListRepositoryImpl implements ReviewListRepository, OnNetworkNotConnectedListener {

    private MutableLiveData<ReviewResponse> reviewResponseLiveData;

    @Override
    public MutableLiveData<String> getErrorMessage() {
        return errorMessage;
    }

    private MutableLiveData<String> errorMessage;
    private MultiLanguageSupport multiLanguageSupport;
    private ReviewListAPIService reviewListAPIService;
    private Gson gson;
    private Context context;
    private int requestId;

    public ReviewListRepositoryImpl(Context context) {
        this.reviewResponseLiveData = new MutableLiveData<>();
        this.errorMessage = new MutableLiveData<>();
        this.context = context;
        this.reviewListAPIService = new ReviewListAPIService(context);
        this.reviewListAPIService.setNetworkNotConnectedListener(this);
        this.gson = new Gson();
        this.multiLanguageSupport = MultiLanguageSupport.getInstance(context);
    }

    @Override
    public LiveData<ReviewResponse> getReviewList() {

        int requestIdLocal = getRequestId();

        reviewListAPIService.getReviews(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (requestId != requestIdLocal) {
                    return;
                }

                if (response.body() != null && response.isSuccessful()) {
                    try {
                        String responsebody = response.body().string();
                        ReviewResponse reviewResponse = gson.fromJson(responsebody, ReviewResponse.class);
                        reviewResponseLiveData.postValue(reviewResponse);
                    } catch (IOException e) {
                        e.printStackTrace();
                        errorMessage.postValue(multiLanguageSupport.getLabel(UILabelsKeys.SOMETHING_WENT_WRONG));
                    }
                } else {
                    String errMsg = NetworkFactory.getErrorMessage(response);
                    if (TextUtils.isEmpty(errMsg)) {
                        errMsg = multiLanguageSupport.getLabel(UILabelsKeys.SOMETHING_WENT_WRONG);
                    }
                    errorMessage.postValue(errMsg);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                errorMessage.postValue(multiLanguageSupport.getLabel(UILabelsKeys.SOMETHING_WENT_WRONG));
            }
        });

        return reviewResponseLiveData;
    }

    public int getRequestId() {
        requestId++;
        return requestId;
    }


    @Override
    public void onNetworkNotConnected() {
        errorMessage.postValue(multiLanguageSupport.getLabel(UILabelsKeys.INTERNET_CONNECTION));
    }

}
