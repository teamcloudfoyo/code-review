package com.way.app.reviews.view.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.way.app.reviews.model.ReviewResponse;
import com.way.app.reviews.service.repository.ReviewListRepository;

public class ReviewListViewModel extends ViewModel {

    private ReviewListRepository reviewListRepository;
    private LiveData<String> errorStringLiveData;
    private LiveData<ReviewResponse> reviewResponseLiveData = new MutableLiveData<>();

    public void setReviewListRepository(ReviewListRepository reviewListRepository) {
        this.reviewListRepository = reviewListRepository;
    }

    public LiveData<ReviewResponse> getReviewLiveData() {
        reviewResponseLiveData = reviewListRepository.getReviewList();
        return reviewResponseLiveData;
    }

    public LiveData<String> getErrorMessage() {
        errorStringLiveData = reviewListRepository.getErrorMessage();
        return errorStringLiveData;
    }

}
