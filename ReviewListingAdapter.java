package com.way.app.reviews.adapter;

import android.os.Build;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.way.app.analytics.AnalyticsManager;
import com.way.app.dashboard.view.activity.DashboardActivity;
import com.way.app.helper.UserStateManager;
import com.way.app.multi_language.MultiLanguageSupport;
import com.way.app.parking.viewmodel.ItemParkingViewModel;
import com.way.app.reviews.model.ReviewResponse;
import com.way.app.util.Log;
import com.way.app.util.ReadMoreReadLess;
import com.way.app.util.ShowMoreTextView;
import com.way.app.util.UILabelsKeys;
import com.way.app.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import way.com.BuildConfig;
import way.com.R;
import way.com.databinding.LayoutLoadingItemBinding;
import way.com.databinding.ListItemReviewBinding;

import static com.way.app.analytics.events.constant.EventNameConstants.APP_REVIEW_VISIT;

public class ReviewListingAdapter  extends RecyclerView.Adapter {
    public final int VIEW_TYPE_ITEM = 1;
    public final int VIEW_TYPE_LOADING = 0;
    private List<ReviewResponse.Review> mReviewArrayList;
    private OnReviewListClickListener onReviewListClickListener;
    private DashboardActivity dashboardActivity;
    private MultiLanguageSupport multiLanguageSupport;

    public ReviewListingAdapter(DashboardActivity dashboardActivity) {
        multiLanguageSupport = MultiLanguageSupport.getInstance(dashboardActivity);
        this.mReviewArrayList = new ArrayList<>();
        this.dashboardActivity = dashboardActivity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                LayoutLoadingItemBinding layoutLoadingItemBinding =
                        LayoutLoadingItemBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false);
                return new LoadingViewHolder(layoutLoadingItemBinding);
            case 1:
                ListItemReviewBinding listItemReviewBinding
                        = ListItemReviewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new ViewHolder(listItemReviewBinding);

            default:
                LayoutLoadingItemBinding layoutLoadingItemBinding1 =
                        LayoutLoadingItemBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false);
                return new LoadingViewHolder(layoutLoadingItemBinding1);
        }

    }

    @Override
    public int getItemViewType(int position) {

        if(mReviewArrayList.get(position) == null){
            return  VIEW_TYPE_LOADING;
        }
        else {
            return VIEW_TYPE_ITEM;
        }

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ViewHolder) {
            ((ViewHolder) viewHolder).bind(mReviewArrayList.get(position));
            setAnimation(((ViewHolder) viewHolder).mMainLayout);
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onReviewListClickListener != null) {
                    onReviewListClickListener.onReviewListClick(mReviewArrayList.get(position));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mReviewArrayList.size() > 0 ? mReviewArrayList.size() : 0;
    }

    public void setOnReviewListClickListener(OnReviewListClickListener onReviewListClickListener) {
        this.onReviewListClickListener = onReviewListClickListener;
    }

    public interface OnReviewListClickListener {
        void onReviewListClick(ReviewResponse.Review review);
    }

    public void addData(@Nullable List<ReviewResponse.Review> reviews) {
        List<ReviewResponse.Review> mList = new ArrayList<>(reviews);
        int size = mReviewArrayList.size();
        mReviewArrayList.addAll(mList);
        notifyItemRangeInserted(size, mList.size());
    }



    private class ViewHolder extends RecyclerView.ViewHolder {
        ListItemReviewBinding listItemReviewBinding;
        public ConstraintLayout mMainLayout;

        public ViewHolder(ListItemReviewBinding listItemReviewBinding) {
            super(listItemReviewBinding.getRoot());
            this.listItemReviewBinding = listItemReviewBinding;
            mMainLayout = listItemReviewBinding.mainReviewLayout;
        }

        public void bind(ReviewResponse.Review review) {
            int maxLines = 4;
            listItemReviewBinding.setReview(review);

            if (Build.VERSION.SDK_INT >= 24) {
                Spanned htmlAsSpanned = Html.fromHtml(review.getReview(), Html.FROM_HTML_MODE_LEGACY);
                listItemReviewBinding.tvUserReview.setText(htmlAsSpanned.toString());
            } else {
                Spanned htmlAsSpanned = Html.fromHtml(review.getReview());
                listItemReviewBinding.tvUserReview.setText(htmlAsSpanned.toString());
            }


            if (review.getSourceImgURL() != null && !TextUtils.isEmpty(review.getSourceImgURL())) {
                listItemReviewBinding.ivReviewFrom.setVisibility(View.VISIBLE);
                listItemReviewBinding.layoutSource.setVisibility(View.GONE);
                String imageUrl = BuildConfig.API_URL.concat(Utils.URL_IMAGE.concat(Utils.URL_TESTIMONY_SOURCE_IMAGE.concat(review.getSourceImgURL())));
                Glide.with(dashboardActivity)
                        .load(imageUrl)
                        .signature(new StringSignature(Utils.getDiskCacheSignature()))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(R.mipmap.ic_logo_google)
                        .placeholder(R.mipmap.ic_logo_google).dontAnimate()
                        .into(listItemReviewBinding.ivReviewFrom);
            }else {
                listItemReviewBinding.ivReviewFrom.setVisibility(View.INVISIBLE);
                listItemReviewBinding.layoutSource.setVisibility(View.VISIBLE);
            }


            listItemReviewBinding.tvUserReview.setTag(listItemReviewBinding.tvUserReview.getText());

            ReadMoreReadLess readMoreReadLess = new ReadMoreReadLess(maxLines);
            if(review.isCollpase()){
                readMoreReadLess.makeTextViewResizable(listItemReviewBinding.tvUserReview, maxLines, "..."
                                + multiLanguageSupport.getLabel(UILabelsKeys.READ_MORE),
                        true);
            }
            else{
                readMoreReadLess.makeTextViewResizable(listItemReviewBinding.tvUserReview, -3, "..." +
                                multiLanguageSupport.getLabel(UILabelsKeys.READ_LESS),
                        false);
            }

            readMoreReadLess.onClickable(new ReadMoreReadLess.onClickable() {
                @Override
                public void onCollapse(boolean isCollpase) {
                    mReviewArrayList.get(mReviewArrayList.indexOf(review)).setCollpase(isCollpase);
                }
            });

            listItemReviewBinding.tvUserReview.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {


                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            if (listItemReviewBinding.tvUserReview.getLineCount() >= maxLines &&
                                    listItemReviewBinding.tvUserReview.getText().toString().endsWith(multiLanguageSupport.getLabel(UILabelsKeys.READ_LESS))) {
                                Log.d("read more", getAdapterPosition() + "clicked");
                                trackCleverTapEvent(getAdapterPosition());
                            }


                        }
                    }, 300);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        public void clearAnimation() {

            mMainLayout.clearAnimation();
        }
    }

    private void trackCleverTapEvent(int position) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("ClickOnMore", String.valueOf(position + 1));
        map.put("User_Login_Type", UserStateManager.isUserLoggedIn() ? "Logged-In" : "Anonymous");
        AnalyticsManager.getInstance().logEventWithName(APP_REVIEW_VISIT, map);
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        private LayoutLoadingItemBinding mLayoutLoadingItemBinding;

        /**
         * Constructor
         *
         * @param layoutLoadingItemBinding List Item Binding Object
         */
        LoadingViewHolder(LayoutLoadingItemBinding layoutLoadingItemBinding) {
            super(layoutLoadingItemBinding.linearLayoutLoading);
            mLayoutLoadingItemBinding = layoutLoadingItemBinding;
        }

        /**
         * Binding Method
         */
        void bindData() {
            if (mLayoutLoadingItemBinding.getLoadingViewModel() == null) {
                mLayoutLoadingItemBinding.setLoadingViewModel(
                        new ItemParkingViewModel(itemView.getContext(), mLayoutLoadingItemBinding));
            }
        }
    }

    /**
     * add item for showing progress
     */
    public void showLoadMoreProgressBar() {
        mReviewArrayList.add(null);
        notifyItemInserted(mReviewArrayList.size() - 1);
    }

    /**
     * hide load more progress icon
     */
    public void hideLoadMoreProgressbar() {
        //Remove loading item
        if (mReviewArrayList.size() > 0 &&
                getItemViewType(mReviewArrayList.size() - 1) == VIEW_TYPE_LOADING) {
            mReviewArrayList.remove(mReviewArrayList.size() - 1);
            notifyItemRemoved(mReviewArrayList.size());
        }
    }

    private void setAnimation(View view) {
        AnimationSet animationSet = new AnimationSet(true);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.2f, 1.0f);
        alphaAnimation.setDuration(250);
        view.startAnimation(alphaAnimation);
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.75f, 1.0f, 0.75f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setDuration(250);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        view.startAnimation(animationSet);
    }

    @Override
    public void onViewDetachedFromWindow(final RecyclerView.ViewHolder holder) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).clearAnimation();
        }
    }
}
