package com.way.app.reviews.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ReviewResponse implements Parcelable {

    public static final Parcelable.Creator<ReviewResponse> CREATOR = new Parcelable.Creator<ReviewResponse>() {
        @Override
        public ReviewResponse createFromParcel(Parcel source) {
            return new ReviewResponse(source);
        }

        @Override
        public ReviewResponse[] newArray(int size) {
            return new ReviewResponse[size];
        }
    };
    @SerializedName("apiStatus")
    @Expose
    private String apiStatus;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("response")
    @Expose
    private ArrayList<Review> reviews = null;

    public ReviewResponse() {
    }

    protected ReviewResponse(Parcel in) {
        this.apiStatus = in.readString();
        this.message = in.readString();
        this.reviews = in.createTypedArrayList(Review.CREATOR);
    }

    public String getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(String apiStatus) {
        this.apiStatus = apiStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Review> getReviews() {
        return reviews;
    }

    public void setReviews(ArrayList<Review> reviews) {
        this.reviews = reviews;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.apiStatus);
        dest.writeString(this.message);
        dest.writeTypedList(this.reviews);
    }

    public static class Review implements Parcelable {

        public static final Creator<Review> CREATOR = new Creator<Review>() {
            @Override
            public Review createFromParcel(Parcel source) {
                return new Review(source);
            }

            @Override
            public Review[] newArray(int size) {
                return new Review[size];
            }
        };
        @SerializedName("testimonialID")
        @Expose
        private String testimonialID;
        @SerializedName("reviewerName")
        @Expose
        private String Name;
        @SerializedName("review")
        @Expose
        private String Review;
        @SerializedName("rating")
        @Expose
        private float reviewCount;
        @SerializedName("source")
        @Expose
        private String source;
        @SerializedName("sourceImgURL")
        @Expose
        private String sourceImgURL;

        boolean isCollpase = true;

        public String getTestimonialID() {
            return testimonialID;
        }

        public void setTestimonialID(String testimonialID) {
            this.testimonialID = testimonialID;
        }

        public Review() {
        }

        public boolean isCollpase() {
            return isCollpase;
        }

        public void setCollpase(boolean collpase) {
            isCollpase = collpase;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getReview() {
            return Review;
        }

        public void setReview(String review) {
            Review = review;
        }

        public float getReviewCount() {
            return reviewCount;
        }

        public void setReviewCount(int reviewCount) {
            this.reviewCount = reviewCount;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getSourceImgURL() {
            return sourceImgURL;
        }

        public void setSourceImgURL(String sourceImgURL) {
            this.sourceImgURL = sourceImgURL;
        }

        protected Review(Parcel in) {
            this.Name = in.readString();
            this.Review = in.readString();
            this.source = in.readString();
            this.sourceImgURL = in.readString();
            this.reviewCount = in.readFloat();
            this.isCollpase = (Boolean) in.readValue(Boolean.class.getClassLoader());

        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.Name);
            dest.writeString(this.Review);
            dest.writeString(this.source);
            dest.writeString(this.sourceImgURL);
            dest.writeFloat(this.reviewCount);
            dest.writeValue(this.isCollpase);

        }
    }
}
